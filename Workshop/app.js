var express = require("express");
var app = express();
app.get("/ ", (req, res, next) => {
    const message = req.query.message || 'World'
    res.json({"response": 'Hello ${message}'})
});
app.listen(3000, () => {
 console.log("Server running on port 3000");
});

