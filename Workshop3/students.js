const express = require('express');
const mongoose = require('mongoose');
const config = require('config');
const studentSchema = require('./models/students');

const dbServerURL = config.get('mongoDbURL');
mongoose.connect(dbServerURL, { useNewUrlParser: true, useCreateIndex: true, useFindAndModify:false })
.then(() => console.log('mongo Connect'))
.catch(err => console.log('Error connecting: ' + err));

const app = express(); 
const port = config.get('serverPort');
app.listen(port, () =>{
    console.log('Server has been started...');
}); 

app.get('/', (req, res) => {
    res.send('Esto es una pagina')
});

app.get('/api/student/all',(req, res) =>{ 
    studentSchema.find()
    .then(items =>{
        res.status(200).send(items);
    })
    .catch(err =>{
        res.status(500).send('Error: ' + err);
    })
});